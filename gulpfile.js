var gulp = require('gulp');
var del = require('del');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var sourcemaps = require('gulp-sourcemaps');
var buffer = require('vinyl-buffer');
var filter = require('gulp-filter');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var jscs = require('gulp-jscs');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var browserSync = require('browser-sync').create();

var cfg = {
  exportAs: 'thirdFloor.iconBuilder',
  distFileName: 'icon-builder.js'
};

function appJsBundler() {
  return browserify({
    entries: [
      './src/index.js'
    ],
    transform: [],
    debug: true,
    cache: {},
    packageCache: {},
    standalone: cfg.exportAs
  });
}

function makeAppJs(bundler) {
  return bundler.bundle()
    .pipe(source(cfg.distFileName))
    .pipe(buffer())
    .pipe(sourcemaps.init({
      loadMaps: true
    }))
    .pipe(sourcemaps.write('./'));
}

gulp.task('_jscs', function() {
  return gulp.src('src/**/*.js')
    .pipe(jscs());
});

gulp.task('_jshint', function() {
  return gulp.src('src/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter(stylish));
});

gulp.task('_js', ['_jscs', '_jshint'], function() {
  var dest = 'dist';
  return makeAppJs(appJsBundler())
    .pipe(gulp.dest(dest))
    .pipe(filter('*.js'))
    .pipe(uglify({
      mangle: true
    }))
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest(dest));
});

gulp.task('demo', ['_js'], function() {
  browserSync.init({
    server: {
      baseDir: './demo',
      routes: {
        '/dist': './dist'
      }
    },
    files: [
      'demo/**/*',
      'dist/**/*'
    ],
    open: false,
    notify: false,
    minify: false,
    host: '0.0.0.0'
  });
});

gulp.task('default', [
  '_js'
]);
