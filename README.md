# Icon Builder

It uses HTML Canvas to draw icons.

---

## How to build

Run in your console
`gulp`

## How run demo

Run in your console
`gulp demo`

---

## How use it

### Using global scope

```
var iconBuilder = window.thirdFloor.iconBuilder;
// do something
```

### Using AMD

```
require(['some-path/icon-builder.js'], function(iconBuilder) {
  // do something
});
```

### Using CommonJS

```
var iconBuilder = require('third-floor-icon-builder');
// do something
```

---

## API

+ CANVAS_DEFAULT_WIDTH
It contains default width of canvas, 124px

+ CANVAS_DEFAULT_WIDTH
It contains default height of canvas, 124px

+ build(cfg)
It build the icon and returns the string that contains "data:URI" representation of the icon.
It accepts a single parameter - the configuration object. See `src/defaultConfig.js` for a
complete list of the properties in this object. You can specify only those properties that you want to override.

---

## Configuration object for `build(...)` function

See the default values in `src/defaultConfig.js`

+ __canvas__ - this section contains settings for HTML Canvas element
    + __width__ - the width of HTML Canvas element
    + __height__ - the height of HTML Canvas element

+ __backgroundShape__ - this section contains settings for _background shape_. The _background shape_ is the combination of background circle and triangle at the bottom
    + __backgroundColor__ - the color of fill
    + __circle__ - this section contains settings for _background circle_
        + __radius__ - the radius of _background circle_
    + __triangle__ - this section contains settings for _triangle at the bottom_
        + __width__ - the width of triangle
        + __height__ - the height of triangle

+ __innerCircle__ - this section contains settings for _inner circle_
    + __radius__ - the radius of _inner circle circle_
    + __backgroundColor__ - the color of fill

+ __innerText__ - this section contains settings for inner text (i.e. numbers inside icon)
    + __content__ - the text
    + __color__ - the color of the text
    + __style__ - this section contains settings for [font](https://developer.mozilla.org/en-US/docs/Web/CSS/font) CSS property of the text
        + __fontStyle__ - [font-style](https://developer.mozilla.org/en-US/docs/Web/CSS/font-style)
        + __fontVariant__ - [font-variant](https://developer.mozilla.org/en-US/docs/Web/CSS/font-variant)
        + __fontWeight__ - [font-weight](https://developer.mozilla.org/en-US/docs/Web/CSS/font-weight)
        + __fontSize__ - [font-size](https://developer.mozilla.org/en-US/docs/Web/CSS/font-size)
        + __fontFamily__ - [font-family](https://developer.mozilla.org/en-US/docs/Web/CSS/font-family)
    + __yOffset__ - it contains a numeric value that allow more accurately center the text vertically

+ __outerText__ - this section contains settings for outer text (i.e. rounded text over icon)
    + __content__ - the text
    + __color__ - the color of the text
    + __style__ - this section contains settings for [font](https://developer.mozilla.org/en-US/docs/Web/CSS/font) CSS property of the text
        + __fontStyle__ - [font-style](https://developer.mozilla.org/en-US/docs/Web/CSS/font-style)
        + __fontVariant__ - [font-variant](https://developer.mozilla.org/en-US/docs/Web/CSS/font-variant)
        + __fontWeight__ - [font-weight](https://developer.mozilla.org/en-US/docs/Web/CSS/font-weight)
        + __fontSize__ - [font-size](https://developer.mozilla.org/en-US/docs/Web/CSS/font-size)
        + __fontFamily__ - [font-family](https://developer.mozilla.org/en-US/docs/Web/CSS/font-family)
    + __padding__ - the distance between the top of the background circle and the bottom of the text
    + __arc__ - maximum arc angle in degrees

---

## If you want to use web fonts

You need to preload the fonts before you call `iconBuilder.build()` in order to HTML Canvas
had the opportunity to draw the text using web fonts. This module does not depend on any fonts
loader and it not depend on any method of preloading fonts. You decide yourself how to download fonts
and when to call `iconBuilder.build()`.
For example, the demo page use [Google WebFontLoader](https://github.com/typekit/webfontloader).

---

## Demo

Run

```
gulp demo

```

It will launch the local web server on [http://localhost:3000](http://localhost:3000).

The pages

+  [http://localhost:3000](http://localhost:3000) - it show the usage of icon-builder for Google Map
+  [http://localhost:3000/generated-icon.html](http://localhost:3000/generated-icon.html) - it shows generated icon in full size
