(function() {
  
  // config for icon
    iconCfg = {
      innerText: {
        content: '4',
        style: {
          fontFamily: 'Signika'
        },
        yOffset: 3
      },
      outerText: {
        content: 'Outer Text',
        arc: 270,
        style: {
          fontFamily: 'Signika'
        }
      }
    };
  
  function start() {
    var root = document.querySelector('#root');
    
    var img = document.createElement('img');
    img.src = window.thirdFloor.iconBuilder.build(iconCfg);
    root.appendChild(img);
  }
  
  // first we need preload the webfonts which we will use in icons
  window.WebFont.load({
    google: {
      families: [
        'Signika:600'
      ]
    },
    active: function() {
      // start everything when all fonts loaded
      start();
    }
  });
})(); 
