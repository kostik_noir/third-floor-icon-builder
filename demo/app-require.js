/*
 * The purpose of this code is to simply show how to use this module in Require.js
 */
require(['dist/icon-builder.js'], function(iconBuilder) {
  console.log(iconBuilder);
});
