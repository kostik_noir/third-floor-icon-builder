(function() {
  function start() {
    // declare points
    var points = [
      {
        lat: 40.7141667,
        lng: -74.0063889,
        innerText: '1',
        outerText: 'New York'
      },
      {
        lat: 39.9522222,
        lng: -75.1641667,
        innerText: '2',
        outerText: 'Philadelphia'
      },
      {
        lat: 41.298319,
        lng: -72.9290959,
        innerText: '3',
        outerText: 'New Haven'
      }
    ];

    // create Google map
    var map = new google.maps.Map(document.querySelector('#root'), {
      center: points[0],
      zoom: 8,
      mapTypeId: google.maps.MapTypeId.SATELLITE
    });

    var bounds = new google.maps.LatLngBounds();

    var latLng;
    var icon;
    var iconCfg;
    var marker;

    // size of icon on the map
    var scaleRatio = 0.5;
    var originalWidthOfIcon = window.thirdFloor.iconBuilder.CANVAS_DEFAULT_WIDTH;
    var originalHeightOfIcon = window.thirdFloor.iconBuilder.CANVAS_DEFAULT_HEIGHT;
    var scaledWidthOfIcon = originalWidthOfIcon * scaleRatio;
    var scaledHeightOfIcon = originalHeightOfIcon * scaleRatio;

    var anchorOfIcon = new google.maps.Point(scaledWidthOfIcon * 0.5, scaledHeightOfIcon + 10);
    var scaledSizeOfIcon = new google.maps.Size(scaledWidthOfIcon, scaledHeightOfIcon);

    // create markers
    var markers = [];
    points.forEach(function(point) {
      latLng = new google.maps.LatLng(point.lat, point.lng);
      bounds.extend(latLng);

      // config for icon
      iconCfg = {
        innerText: {
          content: point.innerText,
          style: {
            fontFamily: 'Signika'
          }
        },
        outerText: {
          content: point.outerText,
          arc: 270,
          style: {
            fontFamily: 'Signika'
          }
        }
      };

      icon = {
        url: window.thirdFloor.iconBuilder.build(iconCfg),
        scaledSize: scaledSizeOfIcon,
        anchor: anchorOfIcon
      };

      marker = new google.maps.Marker({
        position: latLng,
        map: map,
        icon: icon
      });
      markers.push(marker);
    });

    map.fitBounds(bounds);
  }

  // first we need preload the webfonts which we will use in icons
  window.WebFont.load({
    google: {
      families: [
        'Signika:600'
      ]
    },
    active: function() {
      // start everything when all fonts loaded
      start();
    }
  });
})();
