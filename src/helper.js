var api = {};

/**
 * @param {CanvasRenderingContext2D} context
 * @param {Number} centerX
 * @param {Number} centerY
 * @param {Number} radius
 * @param {String} bgColor
 */
api.drawCircle = function(context, centerX, centerY, radius, bgColor) {
  context.save();
  context.fillStyle = bgColor;

  context.beginPath();
  context.arc(centerX, centerY, radius, 0, Math.PI / 180 * 360, true);
  context.closePath();

  context.fill();
  context.restore();
};

/**
 * @param {CanvasRenderingContext2D} context
 * @param {Array} points
 * @param {String} bgColor
 */
api.drawPath = function(context, points, bgColor) {
  context.save();
  context.fillStyle = bgColor;

  context.beginPath();
  var count = points.length;
  for (var i = 0; i < count; i++) {
    if (i === 0) {
      context.moveTo(points[i].x, points[i].y);
      continue;
    }
    context.lineTo(points[i].x, points[i].y);
  }
  context.closePath();

  context.fill();
  context.restore();
};

/**
 * @param {Object} styles
 * @returns {String}
 */
api.fontStylesToCanvasFont = function(styles) {
  return [
    styles.fontStyle || 'normal',
    styles.fontVariant || 'normal',
    styles.fontWeight || 'normal',
    styles.fontSize || '',
    styles.fontFamily || ''
  ].join(' ');
};

/**
 * @param {CanvasRenderingContext2D} context
 */
api.startShadow = function(context) {
  context.save();
  context.shadowColor = 'rgba(0, 0, 0, 0.5)';
  context.shadowBlur = 3;
  context.shadowOffsetX = 2;
  context.shadowOffsetY = 2;
};

/**
 * @param {CanvasRenderingContext2D} context
 */
api.endShadow = function(context) {
  context.restore();
};

module.exports = api;
