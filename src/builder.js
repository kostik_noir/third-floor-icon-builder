var extend = require('extend');
var helper = require('./helper');
var defaultConfig = require('./defaultConfig');

module.exports = {
  /**
   * Default width of the canvas
   * @type {Number}
   */
  CANVAS_DEFAULT_WIDTH: defaultConfig.canvas.width,

  /**
   * Default height of the canvas
   * @type {Number}
   */
  CANVAS_DEFAULT_HEIGHT: defaultConfig.canvas.height,

  /**
   * @param {Object} cfg
   * @returns {String}
   */
  build: function(cfg) {
    cfg = cfg || {};
    cfg = extend(true, {}, defaultConfig, cfg);

    // --------------------------------
    // options
    // --------------------------------
    // canvas
    var canvasWidth = cfg.canvas.width;
    var canvasHeight = cfg.canvas.height;

    // background shape
    var backgroundColor = cfg.backgroundShape.backgroundColor;

    var outerCircleRadius = cfg.backgroundShape.circle.radius;

    var triangleWidth = cfg.backgroundShape.triangle.width;
    var triangleHeight = cfg.backgroundShape.triangle.height;

    // inner circle
    var innerCircleRadius = cfg.innerCircle.radius;
    var innerCircleBgColor = cfg.innerCircle.backgroundColor;

    // inner text
    var innerTextContent = cfg.innerText.content;
    var innerTextColor = cfg.innerText.color;
    var innerTextStyle = cfg.innerText.style;
    var innerTextYOffset = cfg.innerText.yOffset;

    // outer text
    var outerTextPadding = cfg.outerText.padding;
    var outerTextContent = cfg.outerText.content;
    var outerTextColor = cfg.outerText.color;
    var outerTextArc = cfg.outerText.arc;
    var outerTextStyle = cfg.outerText.style;

    // --------------------------------
    // calculated values
    // --------------------------------
    var centerX = canvasWidth * 0.5;
    var centerY = canvasHeight * 0.5;

    // --------------------------------
    // canvas
    // --------------------------------
    var canvas = document.createElement('canvas');
    canvas.setAttribute('width', canvasWidth.toString());
    canvas.setAttribute('height', canvasHeight.toString());
    var context = canvas.getContext('2d');

    // --------------------------------
    // tasks
    // --------------------------------
    function renderOuterTextTask() {
      context.save();

      helper.startShadow(context);

      context.font = helper.fontStylesToCanvasFont(outerTextStyle);
      context.fillStyle = outerTextColor;

      var outerTextRadius = outerCircleRadius + outerTextPadding;

      var lengthOfMaxArc = Math.PI * outerTextRadius * outerTextArc / 180;

      var text = outerTextContent;

      var i;

      // cut too long text
      var ellipsis = '...';
      if (context.measureText(text).width > lengthOfMaxArc) {
        do {
          text = text.substring(0, text.length - 1);
          if (text.length === 0) {
            break;
          }
        } while (context.measureText(text + ellipsis).width > lengthOfMaxArc);
        text = text + ellipsis;
      }

      var lengthOfWholeText = context.measureText(text).width;
      var angleForWholeText = lengthOfWholeText * 180 / (Math.PI * outerTextRadius);
      var startAngle = -angleForWholeText * 0.5;

      var countOfLetters = text.length;

      var lengthsOfLetters = [];
      var testStr = '';
      for (i = 0; i < countOfLetters; i++) {
        testStr += text[i];
        var totalWidth = context.measureText(testStr).width;
        var widthOfLetter = context.measureText(text[i]).width;
        var offsetOfCenter = totalWidth - widthOfLetter * 0.5;
        lengthsOfLetters.push(offsetOfCenter);
      }

      context.translate(centerX, centerY);

      var currentAngle = startAngle;
      var widthRatio;
      var anglePerLetter;
      var radians;
      for (i = 0; i < countOfLetters; i++) {
        widthRatio = lengthsOfLetters[i] / lengthOfWholeText;
        anglePerLetter = widthRatio * angleForWholeText;
        currentAngle = startAngle + anglePerLetter;
        radians = currentAngle * Math.PI / 180;

        context.save();
        context.rotate(radians);

        context.textAlign = 'center';
        context.fillText(text[i], 0, -outerTextRadius);

        context.restore();
      }

      helper.endShadow(context);

      context.restore();
    }

    function renderTriangleTask() {
      context.save();

      var triangleLeftX = centerX - triangleWidth * 0.5;
      var triangleRightX = triangleLeftX + triangleWidth;
      var triangleTopY = centerY + innerCircleRadius;
      var triangleBottomY = triangleTopY + triangleHeight;

      var trianglePoints = [
        {
          x: triangleLeftX,
          y: triangleTopY
        },
        {
          x: triangleRightX,
          y: triangleTopY
        },
        {
          x: centerX,
          y: triangleBottomY
        }
      ];

      context.fillStyle = backgroundColor;

      context.beginPath();
      var count = trianglePoints.length;
      for (var i = 0; i < count; i++) {
        if (i === 0) {
          context.moveTo(trianglePoints[i].x, trianglePoints[i].y);
          continue;
        }
        context.lineTo(trianglePoints[i].x, trianglePoints[i].y);
      }
      context.closePath();
      context.fill();

      context.restore();
    }

    function renderBackgroundTask() {
      context.save();

      helper.startShadow(context);
      renderTriangleTask();
      helper.drawCircle(context, centerX, centerY, outerCircleRadius, backgroundColor);
      helper.endShadow(context);

      // we need 2nd triangle in order to hide shadow from circle
      renderTriangleTask();

      context.restore();
    }

    function renderInnerCircleTask() {
      helper.drawCircle(context, centerX, centerY, innerCircleRadius, innerCircleBgColor);
    }

    function renderInnerTextTask() {
      context.save();
      context.fillStyle = innerTextColor;
      context.font = helper.fontStylesToCanvasFont(innerTextStyle);
      context.textAlign = 'center';
      context.textBaseline = 'middle';
      context.fillText(innerTextContent, centerX, centerY + innerTextYOffset);
      context.restore();
    }

    // ------------------------------------
    // render everything
    // ------------------------------------
    renderOuterTextTask();
    renderBackgroundTask();
    renderInnerCircleTask();
    renderInnerTextTask();

    return canvas.toDataURL();
  }
};
