module.exports = {
  canvas: {
    width: 124,
    height: 124
  },
  backgroundShape: {
    backgroundColor: '#ffffff',
    circle: {
      radius: 35
    },
    triangle: {
      width: 16,
      height: 26
    }
  },
  innerCircle: {
    radius: 30,
    backgroundColor: '#fe7e55'
  },
  innerText: {
    content: '',
    color: '#ffffff',
    style: {
      fontStyle: 'normal',
      fontVariant: 'normal',
      fontWeight: 'normal',
      fontSize: '50px/normal',
      fontFamily: 'sans-serif'
    },
    yOffset: 0
  },
  outerText: {
    content: '',
    color: '#ffffff',
    style: {
      fontStyle: 'normal',
      fontVariant: 'normal',
      fontWeight: 'normal',
      fontSize: '30px/normal',
      fontFamily: 'sans-serif'
    },
    padding: 8,
    arc: 180
  }
};
